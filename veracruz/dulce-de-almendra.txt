Esta receta mexicana de Dulce de almendra de Veracruz, México lleva los siguientes ingredientes:

1/2 kilo almendras peladas y molidas
1/2 litro de agua
8 yemas de huevo
1 raja de vainilla azúcar
cerezas en almíbar
Para preparar dulce de almendra hay que preparar miel con azúcar, agua y la raja de vainilla. Retirar del fuego cuando esté a punto de hebra e incorporar las almendras; batir constantemente, dejar enfriar. Batir las yemas a punto de cordón; añadir la preparación anterior sin dejar de batir. Poner el recipiente en el fuego; mover constantemente para que no se pegue. Retirar de la lumbre y batir por espacio de veinte minutos, hasta que se aclara y aumenta de tamaño. Servir en un platón y dejarlo enfriar; adornar con cerezas en almíbar. La recezeta alcanza para 8 a 10 raciones.


